package de.vorlesung.parallel.gruppe;

import java.awt.Color;

public class FloydSteinbergThread extends Thread {
	private static int count = 0;
	private int identifier = 0;
	private static int[][] pixel;
	private static boolean[][] allowed;

	public static void setAllowed(boolean[][] allowed) {
		FloydSteinbergThread.allowed = allowed;
	}

	public FloydSteinbergThread() {
		identifier = count;
		count++;
	}

	public static int[][] getPixel() {
		return pixel;
	}

	public static void setPixel(int[][] pixel) {
		FloydSteinbergThread.pixel = pixel;
	}

	@Override
	public void run() {
		int width = pixel.length;
		int height = pixel[0].length;
		for (int y = identifier; y < height; y += count) {
			for (int x = 0; x < width; x++) {
				while (!allowed[x][y]) {
					try {
						Thread.sleep(0);
						// System.out.println(identifier + ": waiting" + x + "/"
						// + y);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				floydSteinberg(x, y);
			}
			if (y < height - 1) {
				allowed[width - 3][y + 1] = true;
				allowed[width - 2][y + 1] = true;
				allowed[width - 1][y + 1] = true;
			}
		}
		App.finish();
	}

	public void floydSteinberg(int x, int y) {
		int oldpixel = getGrey(pixel[x][y]);
		int newpixel = decideColor(pixel[x][y]);
		pixel[x][y] = newpixel;
		newpixel = (newpixel >> 8) & 0xFF;
		int quant_error = oldpixel - newpixel;
		try {
			pixel[x + 1][y] = addColors(getGrey(pixel[x + 1][y]),
					(int) (quant_error * 7.0 / 16.0));
			// allowed[x + 1][y] = true;
		} catch (Exception e) {
		}
		try {
			pixel[x - 1][y + 1] = addColors(getGrey(pixel[x - 1][y + 1]),
					(int) (quant_error * 3.0 / 16.0));
		} catch (Exception e) {
		}
		try {
			pixel[x][y + 1] = addColors(getGrey(pixel[x][y + 1]),
					(int) (quant_error * 5.0 / 16.0));
		} catch (Exception e) {
		}
		try {
			pixel[x + 1][y + 1] = addColors(getGrey(pixel[x + 1][y + 1]),
					(int) (quant_error * 1.0 / 16.0));
		} catch (Exception e) {
		}
		try {
			allowed[x - 3][y + 1] = true;
		} catch (Exception e) {
		}
	}

	public int decideColor(int rgb) {
		Color black = new Color(0, 0, 0);
		Color white = new Color(255, 255, 255);
		return (getGrey(rgb) > 127) ? white.getRGB() : black.getRGB();
	}

	// http://stackoverflow.com/questions/9131678/convert-a-rgb-image-to-grayscale-image-reducing-the-memory-in-java
	public int getGrey(int rgb) {
		int r = (rgb >> 16) & 0xFF;
		int g = (rgb >> 8) & 0xFF;
		int b = (rgb & 0xFF);
		return (r + g + b) / 3;
	}

	public int addColors(int c1, int c2) {
		int temp = c1 + c2;
		if (temp > 255) {
			temp = 255;
		} else if (temp < 0) {
			temp = 0;
		}
		return new Color(temp, temp, temp).getRGB();
	}

	public static void reset() {
		count = 0;
		pixel = null;
		allowed = null;
	}
}
