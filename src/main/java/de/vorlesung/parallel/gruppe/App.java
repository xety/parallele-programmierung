package de.vorlesung.parallel.gruppe;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * teast
 */
public class App {
	/**
	 * Starts the Application
	 *
	 * @param args
	 *            The Arguments
	 */
	private static int count = 0;

	public static void main(String[] args) {
		if (args.length > 0) {
			int[][] pixel = readImageToArray(args[0]);
			long SingleTime = SingleFloydSteinberg(pixel);
			writeArrayToFile(pixel, "./SingleOut.jpg");
			System.out.println("Single Thread Time: " + SingleTime + " ms");
			FloydSteinbergThread.reset();
			int[][] pixel2 = readImageToArray(args[0]);
			boolean[][] allowed = createBooleanArray(pixel2);
			long MultiTime = MultiFloydSteinberg(pixel2, allowed);
			writeArrayToFile(pixel2, "./MultiOut.jpg");
			System.out.println("Multi Thread Time: " + MultiTime + " ms");
			System.out.println("Time saved: " + (SingleTime - MultiTime)
					+ " ms");
			System.exit(0);
		} else {
			System.out.println("No picture as argument");
		}
	}

	public static long SingleFloydSteinberg(int[][] newpixels) {
		FloydSteinbergThread fst = new FloydSteinbergThread();
		FloydSteinbergThread.setPixel(newpixels);
		int width = newpixels.length;
		int height = newpixels[0].length;
		long start = System.currentTimeMillis();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				fst.floydSteinberg(x, y);
			}
		}
		long stop = System.currentTimeMillis();
		return (stop - start);
	}

	public static long MultiFloydSteinberg(int[][] newpixels,
			boolean[][] allowed) {
		FloydSteinbergThread fst = new FloydSteinbergThread();
		FloydSteinbergThread f2 = new FloydSteinbergThread();
		FloydSteinbergThread f3 = new FloydSteinbergThread();
		FloydSteinbergThread f4 = new FloydSteinbergThread();
		FloydSteinbergThread.setPixel(newpixels);
		FloydSteinbergThread.setAllowed(allowed);
		long start = System.currentTimeMillis();
		fst.start();
		f2.start();
		f3.start();
		f4.start();
		while (count < 4) {
			try {
				Thread.sleep(0);
			} catch (Exception e) {
			}
		}
		long stop = System.currentTimeMillis();
		return (stop - start);
	}

	public static void finish() {
		count++;
	}

	private static int[][] readImageToArray(String filename) {
		try {
			BufferedImage image = ImageIO.read(new File(filename));
			int width = image.getWidth(null);
			int height = image.getHeight(null);
			int[][] newpixels = new int[width][height];
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					newpixels[x][y] = image.getRGB(x, y);
				}
			}
			return newpixels;
		} catch (IOException e) {
			return null;
		}
	}

	private static void writeArrayToFile(int[][] pixel, String filename) {
		try {
			int width = pixel.length;
			int height = pixel[0].length;
			BufferedImage image = new BufferedImage(width, height, 9);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					image.setRGB(x, y, pixel[x][y]);
				}
			}
			ImageIO.write(image, "jpeg", new File(filename));
		} catch (IOException e) {
		}
	}

	private static boolean[][] createBooleanArray(int[][] pixel) {
		int width = pixel.length;
		int height = pixel[0].length;
		boolean[][] allowed = new boolean[width][height];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (y > 0) {
					allowed[x][y] = false;
				} else {
					allowed[x][y] = true;
				}
			}
		}
		return allowed;
	}
}
